#include <iostream>
#include <cstring>
#include <set>
#include <map>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    string str = "string1";
    string str2 = "string2";
    string str3 = str + str2;
    string str_copy = str;

    str3[0] = 'A';
    cout << str3.c_str() << endl;
    cout << str3.length() << endl;
    cout << str3.substr(0,7).c_str() << endl;
    cout << str3.replace(0,7,"ab").c_str() << endl;
    cout << str3.c_str() << endl;

    int offset = str3.find("ab1");
    int length = 2;
    cout << offset << endl;

    set<int> intSet;
    intSet.insert(3);
    intSet.insert(1);
    intSet.insert(2);
    intSet.insert(2);
    set<int>::iterator setIt = intSet.begin();
    for (;setIt!=intSet.end();++setIt)
        cout << *setIt << endl;

    map<string,int> userScore;
    userScore["user1"] = 1;
    userScore["user2"] = 2;
    pair<string,int> pair1;

    //pair1.

    map<string,int>::iterator mapIt = userScore.begin();
    for (;mapIt != userScore.end();++mapIt)
    {
        cout<<(*mapIt).first.c_str()<<": "
           <<(*mapIt).second<< endl;
    }

    userScore["user1"] += 5;
    cout << userScore["user1"] << endl;


    //cout << str3.replace(offset,length,"new_ab").c_str()<<endl;

    return 0;
}
